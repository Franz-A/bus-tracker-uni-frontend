import firebase from "firebase/app"

const app = firebase.initializeApp({
    apiKey: process.env['VUE_APP_FIREBASE_API_KEY'],
    authDomain: process.env["VUE_APP_FIREBASE_AUTH_DOMAIN"],
    projectId: process.env["VUE_APP_FIREBASE_PROJECT_ID"],
    databaseURL: process.env["VUE_APP_FIREBASE_DATABASE"],
    messagingSenderId: process.env["VUE_APP_FIREBASE_MESSAGING_SENDER_ID"],
    appId: process.env["VUE_APP_FIREBASE_APP_ID"],
});
export default app

