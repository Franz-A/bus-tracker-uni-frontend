//config/firebase-auth.js
import app from "./firebase";
import firebase from "firebase/app"
import  "firebase/auth"
const firebaseAuth = app.auth()

firebaseAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL).
then(r =>{
    console.log(r)
}).
catch(reason => {
    console.log(reason)
});


export default firebaseAuth