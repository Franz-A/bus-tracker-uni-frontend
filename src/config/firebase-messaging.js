//config/firebase-messaging.js
import app from "./firebase";
import "firebase/messaging"

const messaging = app.messaging();

messaging.usePublicVapidKey(
    process.env["VUE_APP_PUBLIC_VAPID_KEY"]
);
// 1. Generate a new key pair
// Request Permission of Notifications


Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // [START_EXCLUDE]
        // In many cases once an app has been granted notification permission,
        // it should update its UI reflecting this.
        messaging.getToken().then((token) => {

            console.log(token,"...")
        })
        // [END_EXCLUDE]
    } else {
        console.log('Unable to get permission to notify.');
    }
}).catch((err) => {
    console.log('Unable to get permission to notify.', err);
});

