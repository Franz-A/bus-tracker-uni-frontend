//config/firebase-firestore.js
import app from "./firebase";
import "firebase/database"

const database = app.database();
export default database;
