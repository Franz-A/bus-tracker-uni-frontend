import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@fortawesome/fontawesome-free/css/all.css'
import 'leaflet/dist/leaflet.css'
import './config/firebase'
import './config/firebase-auth'
import './config/firestore'
import './config/firebase-messaging'
import firebaseAuth from "@/config/firebase-auth";

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
