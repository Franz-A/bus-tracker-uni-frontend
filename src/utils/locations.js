export const distance= function(pointA, pointB){
    if(!pointB){
        pointB=[0,0]
    }
    const a = pointA[0] - pointB[0];
    const b = pointA[1] - pointB[1];
    return Math.sqrt(a * a + b * b)
}


export const generateMiddle = function (pointA, pointB) {
    if(!pointB){
        pointB=[0,0]
    }
    const x = pointA[0]+ pointB[0];
    const y = pointA[1] + pointB[1];
    return [x / 2, y / 2]
}

export const reversePoint = function (pointA) {
    return [pointA[1], pointA[0]]
}
